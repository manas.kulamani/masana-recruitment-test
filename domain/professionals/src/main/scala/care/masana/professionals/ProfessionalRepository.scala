package care.masana.professionals

import care.masana.common.Fixtures

class ProfessionalRepository {

  def getAll(): Seq[Professional] = {
    Seq(
      Professional(
        Fixtures.firstProfessionalRef,
        Seq(
          Fixtures.firstProfessionRef,
          Fixtures.secondProfessionRef
        )
      ),
      Professional(
        Fixtures.secondProfessionalRef,
        Seq(
          Fixtures.thirdProfessionRef
        )
      )
    )
  }
}
