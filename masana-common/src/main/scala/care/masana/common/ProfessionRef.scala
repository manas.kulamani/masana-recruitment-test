package care.masana.common

import java.util.UUID

case class ProfessionRef(uuid: UUID)
