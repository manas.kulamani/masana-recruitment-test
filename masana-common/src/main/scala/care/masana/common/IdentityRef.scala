package care.masana.common

import java.util.UUID

trait IdentityRef {
  val uuid:UUID
}
