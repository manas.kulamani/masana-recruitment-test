package care.masana.service

import care.masana.common.ProfessionalRef
import care.masana.notes.Note
import care.masana.professionals.ProfessionalRepository

class NotesApiImpl {

  /**
   * @param professional The reference of the identified professional.
   * @return a collection of notes that should be read by the professional.
   */
  def getNotesFor(
      professional: ProfessionalRef
  ): Response[Seq[Note]] = {
    val professionalRepository = new ProfessionalRepository
    // get all the professions from ProfessionalRepository by the given ProfessionalRef
    val requiredProfessions = professionalRepository
      .getAll()
      .filter(e => e.ref.uuid == professional.uuid)
      .flatMap(_.professions)

    val resultantNotes = for {
      prof <- requiredProfessions
      note <- NotesRepository
        .getAll()
        .filter(note => note.professions.contains(prof))
    } yield note

    Response(200, resultantNotes)
  }

}
