package care.masana.service

case class Response[C](status: Int, content: C)
