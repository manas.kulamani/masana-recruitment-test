# Masana, recruitment test

> This is as Scala + Sbt project used to assess the abilities of a candidate 
> for the backend.

## Assignment

The system has a kind of messaging system via the "notes". A note is basically 
a text written by an _author_ for one or more _professions_. Once created, a 
_note_ can be read by any _professional_ identified in one of the 
_professions_.

Thanks to our OpenAPI specification, one part of our RESTful API can be 
generated. However, we cannot generate the business logic. Thus, we rely on API 
implementations that are injected into the controllers.

Your objective is to complete the NoteAPI implementation so that our service 
will be able to return all notes for one professional.

## Rules

**✅ You can:**
 + create any classes that you think required to implement the solution
   as you like.
 + change the method signatures except the one from the [`NotesApiImpl`](masana-service/src/main/scala/care/masana/service/NotesApiImpl.scala).
 + add any required library (_zio_ is used in our system).
 
**🚫 You cannot:**
 + change the project structure.

## Submission

Once you are done, create a merge-request to the **main** branch.
